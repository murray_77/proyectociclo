<?php
class Validaciones {
    
    //validacion email    
    public function validaEmail($email){
        $regexEm = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $email= filter_var($email, FILTER_SANITIZE_EMAIL);
        
        if(empty($email)||!preg_match($regexEm, $email)){        
            return null;
        }else{            
            return $email;
        }
    }
    
    //validacion y encriptación de pass
    public function validaPass($pass){
        if(empty($pass)){
            return null;
        }else{            
            return sha1($pass);
        }
    }
    
    //generacion pass aleatoria    
    public function generaPass(){
        
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";//salt de la pass
        //Reconstruimos la contraseña segun la longitud que se quiera
        for($i=0;$i<8;$i++) {
           //obtenemos un caracter aleatorio escogido de la cadena de caracteres
           $pass .= substr($str,rand(0,62),1);
        }               
        //Mostramos la contraseña generada t1ALXpuj
        echo 'Password generado: '.$pass;
        
        return sha1($pass);
    }
    
    //validación nombre/apellido    
    public function validaNombre($nombre){
        if(empty($nombre)||!preg_match("/^[a-zA-z\s]*$/", $nombre)){
            return null;
        }else{
            return $nombre;
        }
    }
    
    //validacion dni
    public function validaDni($dni){
        if(empty($dni)||!preg_match("^\d{8}[A-Z]$/", $dni)){
            return null;
        }else{
            return $dni;
        }
    }
    
    //valida grupo
    public function validaGrupo($grupo){
        if(empty($grupo)){
            return null;
        }else{
            return $grupo;
        }
    }
    
    //valida telefono
    public function validaTelefono($telefono){
        if(empty($telefono)||!preg_match("/\d{9}$/", $telefono)){
            return null;
        }else{
            return $telefono;
        }
    }
    
    
}
