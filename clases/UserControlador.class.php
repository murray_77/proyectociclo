<?php
require_once 'DAO.class.php';
include_once 'Cliente.class.php';
require_once 'Entrenador.class.php';
class UserControlador extends DAO{
    
    private static $usuarioGetByDni;
    private static $usuarioGetAll;
    private static $usuarioGetById;
    private static $usuarioLogin = 'SELECT * FROM usuario WHERE email LIKE :email and pass LIKE :pass';    
    private static $eliminarUsuario = 'DELETE FROM usuario WHERE id = :id';
    private static $nuevoUsuario = 'INSERT INTO usuario(dni, nombre, apellidos, pass, email, telefono) VALUES (:dni, :nombre, :apellido, :pass, :email, :telefono)';
    private static $nuevoCliente = 'INSERT INTO cliente (dni_c, fechaNac, direccion, poblacion, cp, grupo, c_medicas, objetivos, anotaciones) VALUES (:dni, :fechaN, :direccion, :poblacion, :cp, :gupo, :cMedicas, :objetivos, :anotaciones)';
    
    //Se recibe la tabla de la que se va a consultar y el tipo de Id o dni según si es cliente o entrenador
    private function setUsuarioGetByDni($tabla, $tipoDni){
        self::$usuarioGetByDni = "SELECT * FROM $tabla INNER JOIN usuario ON $tipoDni = dni WHERE $tipoDni LIKE :dni OR nombre LIKE :nombre";
    }

    private function setUsuarioGetById($tabla, $tipoId){
        self::$usuarioGetById = "SELECT * FROM $tabla INNER JOIN usuario ON $tipoId = id WHERE $tipoId LIKE :id";
    }

    private function setUsuarioGetAll($tabla, $tipoId){
       self::$usuarioGetAll = "SELECT * FROM $tabla INNER JOIN usuario ON id = $tipoId";
    }    
        
    
    //consulta para login
    public function getLogin($email, $pass){

        $paramArray = array (":email" => $email . "%",":pass" => $pass); //Se crea el array de parametros que se van a pasar a DAO
        $resultado = self::executeQuery(self::$usuarioLogin, $paramArray);//Se le pasa la consulta y los parámetros al DAO
        return $resultado;        
    }
    
    
    //consulta para buscador por dni o nombre
    public function getUsuarioByDni($dni, $nombre, $tabla, $tipoDni){
        
        $this->setUsuarioGetByDni($tabla, $tipoDni);
        $paramArray = array (":dni" =>$dni . "%", ":nombre" => $nombre . "%"); 
        $resultado = self::executeQuery(self::$usuarioGetByDni, $paramArray);
        return $resultado;
    }
    
    
    //Consulta de usuario por id (lanzado por un get en la tabla de usuarios)
    public function getUsuarioById($id, $tabla, $tipoId){
        $this->setUsuarioGetById($tabla, $tipoId);
        $paramArray = array(":id"=>$id);
        $resultado = self::executeQuery(self::$usuarioGetById, $paramArray);
        return $resultado;
    }
    
    //consulta para obtener toda la tabla
    public function getAll($tabla, $tipoId){        
        $this->setUsuarioGetAll($tabla, $tipoId);
        $resultado = self::executeQuery(self::$usuarioGetAll, null);
        return $resultado;
    }
    
    public function insertCliente($cliente){
       $paramArray1 = array(":dni"=> $cliente->getDni(), ":nombre" => $cliente->getNombre(), 
                            ":apellido"=> $cliente->getApellido(), ":pass"=>$cliente->getPass(), 
                            ":email"=>$cliente->getEmail(), ":telefono"=>$cliente->getTelefono());      
       
       $paramArray2 = array(":dni" =>$cliente->getDni(), ":gupo"=> $cliente->getGrupoCliente(), 
                            ":fechaN" => $cliente->getFechaN(), "direccion" =>$cliente->getDireccion(),
                            ":poblacion" =>$cliente->getPoblacion(), ":cp"=>$cliente->getCp(), 
                            ":cMedicas"=>$cliente->getCMedicas(), ":objetivos"=>$cliente->getObjetivos(), 
                            ":anotaciones"=>getAnotaciones());
       
       self::executeUpdate(self::$nuevoUsuario, $paramArray1);
       self::executeUpdate(self::$nuevoCliente, $paramArray2);
    }    
    
    //update borrar usuario (solo editable por un entrenador con permisos de admin)
    public function deleteUser($id){
        $paramArray = array(":id"=>$id . "%");
        self::executeUpdate(self::$eliminarUsuario, $paramArray);
    }

    
}
