<?php
include_once 'Persona.class.php';

class Cliente extends Persona{
    private $fechaN;
    private $direccion;
    private $poblacion;
    private $cp;
    private $grupo;
    private $cMedicas;
    private $objetivos;
    private $anotaciones;
    
    
    
    function getFechaN() {
        return $this->fechaN;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getPoblacion() {
        return $this->poblacion;
    }

    function getCp() {
        return $this->cp;
    }
    
    function getGrupoCliente(){
        return $this->grupo;
    }
	
    function getCMedicas(){
            return $this->cMedicas;
    }

    function getObjetivos(){
            return $this->objetivos;
    }

    function getAnotaciones(){
            return $this->anotaciones;
    }
    
    function setFechaN($fechaN) {
        $this->fechaN = $fechaN;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setPoblacion($poblacion) {
        $this->poblacion = $poblacion;
    }

    function setCp($cp) {
        $this->cp = $cp;
    }
    
    function setGrupoCliente($grupo){
        $this->grupo = $grupo;
    }
	
    function setCMedicas($cMedicas){
            $this->cMedicas = $cMedicas;
    }

    function setObjetivos($objetivos){
            $this->objetivos = $objetivos;
    }

    function setAnotaciones($anotaciones){
            $this->anotaciones = $anotaciones;
    }
}
