-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 15-02-2020 a las 10:46:07
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_c` int(11) NOT NULL,
  `dni_c` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `fechaNac` date NOT NULL,
  `direccion` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `poblacion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `cp` int(5) NOT NULL,
  `grupo` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`dni_c`,`id_c`) USING BTREE,
  KEY `id` (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_c`, `dni_c`, `fechaNac`, `direccion`, `poblacion`, `cp`, `grupo`) VALUES
(1, '25896578P', '2019-03-21', 'Su casa', 'En su pueblo', 45789, 'G1'),
(3, '85896587p', '1988-09-08', 'Otra Casa', 'Otro Pueblo', 78521, 'G1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenador`
--

DROP TABLE IF EXISTS `entrenador`;
CREATE TABLE IF NOT EXISTS `entrenador` (
  `id_e` int(11) NOT NULL,
  `dni_e` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `administrador` tinyint(1) NOT NULL,
  `grupo` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`dni_e`,`id_e`) USING BTREE,
  KEY `id_e` (`id_e`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `entrenador`
--

INSERT INTO `entrenador` (`id_e`, `dni_e`, `administrador`, `grupo`) VALUES
(2, '32710406J', 1, 'G1'),
(4, '87854585L', 0, 'G2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'user.png',
  `apellidos` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` bigint(9) NOT NULL,
  `entrenador` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`dni`,`id`) USING BTREE,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla padre de usuarios';

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `dni`, `nombre`, `foto`, `apellidos`, `pass`, `email`, `telefono`, `entrenador`) VALUES
(1, '25896578P', 'Paco', 'user.png', 'Pelaez', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'user@user.com', 888888888, 0),
(2, '32710406J', 'Santiago', 'user.png', 'Garcia Castro', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'santiago@admin.com', 259685478, 1),
(3, '85896587p', 'Juana', 'user.png', 'Paez', '25Lmwsm6Iw2ms', 'user2@user.com', 258965587, 0),
(4, '87854585L', 'Jorge', 'user.png', 'Casal', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'admin2@admin.com', 852654753, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`dni_c`) REFERENCES `usuario` (`dni`) ON DELETE CASCADE,
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`id_c`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `entrenador`
--
ALTER TABLE `entrenador`
  ADD CONSTRAINT `entrenador_ibfk_1` FOREIGN KEY (`dni_e`) REFERENCES `usuario` (`dni`) ON DELETE CASCADE,
  ADD CONSTRAINT `entrenador_ibfk_2` FOREIGN KEY (`id_e`) REFERENCES `usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
