/****************Calendario para citas o agenda(cliente/admin)*****************************/

document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendarioE');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    height: parent,
    editable: true,
    plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
    selectable: true,
    aspectRatio: 2,
    minTime: '08:00:00',
    maxTime: '22:00:00',
    allDaySlot: false,
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek'
    },

    businessHours: {
        daysOfWeek: [1, 2, 3, 4, 5],
        startTime: '08:00',
        endTime: '22:00' 
    },
    
    /*dateClick: function(info) {
      alert('clicked ' + info.dateStr);
    },*/
    select: function(info) {
      alert('selected ' + info.startStr + ' to ' + info.endStr);
    }
  });
  
  calendar.setOption('locale','es');

  calendar.render();
});

/***********************************************Buscador clientes por nombre******************************************************************************/


function buscar(){
    var entrada = document.getElementById("busc");
    var filtro = entrada.value.toUpperCase();
    var tabla = document.getElementById("tabus");
    var fila = tabla.getElementsByTagName("tr");
    var coln;
    var cold;

    for (var i = 0; i < fila.length; i++) {
        coln = fila[i].getElementsByTagName("td")[1];
        cold = fila[i].getElementsByTagName("td")[0];

        if (coln) {
            contN = coln.textContent;
            contD = cold.textContent;

            if (contN.toUpperCase().indexOf(filtro) > -1 || contD.toUpperCase().indexOf(filtro)>-1){
                fila[i].style.display = "";
            }else{
                fila[i].style.display = "none";
            }
        }
    }
}

/******************animación boton toggle nav************************/

function cambiar(x) {
    x.classList.toggle("change");
    }


/******************************validacion formulario*****************************************************/
function validaUsuario(){
    var nombre = document.getElementById("nombre");
    var apellido = document.getElementById("apellido");
    var grupo = document.getElementById("grupo");
    var dni = document.getElementById("dni");
    var telefono = document.getElementById("tele");
    var email = document.getElementById("em");	

    return (vNombre(nombre) || vNombre(apellido) || vGrupo(grupo) || vDni(dni) || vTelefono(telefono) || vEmail(email));
}

function vNombre(nombre){
    if (nombre.value.length === 0||!(/^[a-zA-z\s]*$/.test(nombre.value))){		
        nombre.classList.add("is-invalid");
        document.getElementById("error1").innerHTML = "Campo nombre Erroneo";
        return false;
    }
}


function vDni(dni){
    dni = dni.toUpperCase();
    if (dni.value.length === 0||!(/^\d{8}[A-Z]$/.test(dni.value))){
        dni.classList.add("is-invalid");
        document.getElementById("error3").innerHTML = "Campo dni Erroneo";
        return false;	
    }
}

function vGrupo(grupo){
    if (grupo.value.length === 0){
        grupo.classList.add("is-invalid");
        document.getElementById("error4").innerHTML = "Grupo no seleccionado";
        return false;
    }
}

function vTelefono(telefono){
    if (telefono.value.length === 0||!(/\d{9}$/.test(telefono.value))){
        telefono.classList.add("is-invalid");
        document.getElementById("error5").innerHTML = "Campo teléfono Erroneo";
        return false;
    }
}

function vEmail(email){
    if (email.value.length === 0||!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email.value))){
        email.classList.add("is-invalid");
        document.getElementById("error6").innerHTML = "Campo email Erroneo";
        return false;
    }	
}



/**********************************jquery**********************************************************/
$(document).ready(function(){
    
/**********ancho de la pantalla*****************************/

    var w = $(window).width();
/**********************************smooth scroll****************************************************/

$("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

/**************************scroll del navbar******************************************************/

    if(w>=768){        
        
        $(window).scroll(function(){

            var speed = 700;
            var distanceToTop = $(window).scrollTop();

            var opacity = 0 + distanceToTop / speed;

            $('.backNav').css("opacity", opacity);
            $('.logoNav').css("opacity", opacity);

            if(opacity>0.4){
                $(".enlaceNav").removeClass("en1");
                $(".enlaceNav").addClass("en2");
            }else{
                $(".enlaceNav").removeClass("en2");
                $(".enlaceNav").addClass("en1");
            }
            
        });
    }
    
/*****************************menu movil****************************************/
    
    
    $(".navBoton").click(function(){
        $("#menu").toggleClass("main-nav main-nav-vis");
    });    
    
    
    
/*****************************login modal**************************************/

    $(".modalVentana").hide();
    
    $("#login").click(function(){
       $("#modalLogin").show("fade","slow"); 
    });
    
    $("#cerrar").click(function(){
       $(".modalVentana").fadeOut(); 
    });
    
    $(document).keydown(function(e) {
        if(e.keyCode === 27) {
            $(".modalVentana").fadeOut();
            $("#login").blur();
        }
    });
    
       
    $("#olvidaForm").click(function(){
       $("#modalOlvida").fadeToggle(); 
    });
    
    $(".cerrarForm").click(function(){
       $(".modalVentana").fadeOut(); 
    });
    
    $(document).keydown(function(e) {
        if(e.keyCode === 27) {
            $(".modalVentana").fadeOut();
            $("#login").blur();
        }
    });

    
/**********************************carousel*********************************************************/
    $('.slidePres').slick({
        centerPadding: '10px',
        centerMode: true,
        slidesToshow: 3,
        arrows: true,
        nextArrow: '<button class="anterior">Next</button>',
        prevArrow: '<button class="siguiente">Previous</button>',
        adaptiveHeight: false,
        variableWidth: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '10px',
                slidesToShow: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '10px',
                slidesToShow: 1
            }
        }]
      
    });


    $('.slideBlog').slick({
        centerMode: true,
        centerPadding: '20px',
        arrows: false,
        slidesToShow: 1,
        adaptiveHeight: false,
        variableWidth: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '10px',
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '10px',
              slidesToShow: 1
            }
          }
        ]
    });  

/******************************efecto color formulario*********************************************/

    var original = $(".form-control").css("background-color");
    $(".form-control").on({
        mouseenter: function(){
            $(this).css("background-color","#F7F8E0");
        },

        mouseleave:function(){
            $(this).css("background-color",original);
        }
    });
	
/******************************borrar y restaurar placeholder*****************************************/

    var valor;
    $(".form-control").focus(function(){
        valor = $(this).attr("placeholder");
        $(this).attr("placeholder", "");
    });

    $(".form-control").blur(function(){
        $(this).attr("placeholder", valor);
    });
    

	
/**************************keydown limpia los errores del fomulario*****************************************************/

    $("#nombre, #apellido, #grupo, #dni, #tele, #em, #login, #pass").keydown(function(){
        $(this).removeClass("is-invalid");
        $("#error1, #error2, #error3, #error4, #error5, #error6").hide();
    });
	
	

/****************************************Resize del logo**************************************************************/

    $(window).resize(function(){
        if($(this).outerHeight() < 768 || $(this).outerWidth <768){
            $("#logo").removeClass("logo").addClass("minilogo");		
        }else{
            $("#logo").removeClass("minilogo").addClass("logo");
        }
    });


/*******************************************Resalta la página actual en el nav******************************************/
	
    var titulo = document.title;
    switch(titulo){
        case "Agenda":
            $("#agenda").toggleClass("en2 active");
            break;
        case "Entrenadores":
            $("#entrenadores").toggleClass("en2 active");
            break;        
        case "Datos del Entrenador":
            $("#entrenadores").toggleClass("en2 active");
            break;
        case "Datos del Cliente":
            $("#clientes").toggleClass("en2 active");
            break;
        case "Alta Usuarios":
            $("#clientes").toggleClass("en2 active");
            break;
        case "Clientes":
            $("#clientes").toggleClass("en2 active");
            break;
        case "Alta Entrenadores":
            
            $("#entrenador").addClass("active");
            break;
        
        case "Grupos":
            $("#grupos").addClass("active");
            break;
        case "Calendario":
            $("#calendario").addClass("active");
            break;				
    }

/*****************************ventana modal**************************************************************************/
    
    $('#modal').on('shown.bs.modal', function () {
    $('#modal').trigger('focus');

    });
    
/*************************flecha tabla*********************************************************************/

    $( ".flechaTabla" ).click(function() {
        if (  $( this ).css( "transform" ) === 'none' ){
            $(this).css("transform","rotate(90deg)");
        } else {
            $(this).css("transform","" );
        }
    });
                
/********************************Pantalla datos**************************************/

    $("#botAnotaciones").click(function(){
        $("#botDatos").removeClass("active");
        $("#botAnotaciones").addClass("active");
        $("#datos").hide( "drop", { direction: "left" }, "1000", function(){
            $("#anotaciones").show("drop", { direction: "rigth" }, "1000" );
        });
        
    });
    
    $("#botDatos").click(function(){
        $("#botAnotaciones").removeClass("active");
        $("#botDatos").addClass("active");
        $("#anotaciones").hide("drop", { direction: "rigth" }, "1000", function(){
            $("#datos").show("drop", { direction: "left" }, "1000" );  
        });
            
    });
    
/****************datos nuevo cliente***************************************************/
    $("#anotaciones").hide();
    $("#slide").click(function(){
        if(!$("#anotaciones").is(":visible")){
            $("#datos").hide( "drop", {direction: "left" }, "1000",function(){
                $("#anotaciones").show("drop", {direction: "rigth" }, "1500" ); 
                $("#aceptar").show("drop", {direction: "rigth" }, "1500" );
                $("#slide").html("< Anterior");
            });
        }else{
            $("#aceptar").hide( "drop", { direction: "rigth" }, "1000");
            $("#anotaciones").hide( "drop", { direction: "rigth" }, "1000",function(){ 
                $("#datos").show("drop", { direction: "left" }, "1500" );
                $("#slide").html("Siguiente >");
            });
        }      
    });

    
/*******************************foto usuario con ajax****************************************************************/
    $("#subirfoto").on('submit',(function(e) {
        e.preventDefault();
        $("#message").empty();
        $('#loading').show();
        $.ajax({
            url: "/proyecto2.0/clases/guardaFoto.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data){   // A function to be called if request succeeds

                $('#loading').hide();
                $("#message").html(data);
            }
        });
    }));

    // Function to preview image after validation

    $(function() {
        $("#foto").change(function() {
            $(".invalid-feedback").empty(); // To remove the previous error message
            var foto = this.files[0];
            var imagefile = foto.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
                $('#userfoto').attr('src','/proyecto2.0/img/profiles/user.png');
                $(".invalid-feedback").html("Formato de imagen erroneo").show();
                return false;
            }else{
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoaded(e) {
            $('#userfoto').attr('src', e.target.result);
    };
	 
});


