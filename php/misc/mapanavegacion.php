<?php
    
    session_start();
    require('Smarty.class.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto/clases/Login.class.php';
    $smarty = new Smarty;    
    $smarty->template_dir = '..\vista\templates';
    $smarty->compile_dir = '..\vista\templates_c';
    $smarty->config_dir = '..\vista\configs';
    $smarty->cache_dir = '..\vista\cache';
    
    if(isset($_POST["acceder"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
            $login = new Login();
            $login ->validaLogin($_POST["user"], $_POST["pass"]);            
        }
    
    $smarty->assign("titulo","Mapa de Navegación");
    $smarty->display('mapa.tpl');     
    $smarty->display('principal/footer.tpl');
    
    
    
  