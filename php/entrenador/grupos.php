<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/smarty/libs/Smarty.class.php');
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if($_SESSION["usuario"]->entrenador!=1){
        header('Location: /proyecto/principal/noautorizado.php');
    }
    
    $smarty = new Smarty;    
    $smarty->template_dir = '..\..\vista\templates';
    $smarty->compile_dir = '..\..\vista\templates_c';
    $smarty->config_dir = '..\..\vista\configs';
    $smarty->cache_dir = '..\..\vista\cache';
    

    
    $smarty->assign("titulo","Grupos");
    $smarty->display('principal/head.tpl');
    $smarty->display('entrenador/navEntrenador.tpl');       
       
    $smarty->display('principal/footer.tpl');