<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/UserControlador.class.php');
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if($_SESSION["usuario"]->entrenador!=1){
        header('Location: /proyecto2.0/php/misc/noautorizado.php');
    }
    
    if(isset($_GET["id"])){
        if(isset($_GET["class"])&&$_GET["class"]=="cliente"){        
            $control = new UserControlador();
            $resultado = $control->getUsuarioById($_GET["id"], "cliente", "id_c");
            $usuario = $resultado[0];
            $smarty->assign("titulo","Datos del Cliente");
            $smarty->assign('grupo', $usuario->grupo);    
            $smarty->assign('fechaN', $usuario->fechaNac);
            $smarty->assign('direccion', $usuario->direccion);
            $smarty->assign('poblacion', $usuario->poblacion);
            $smarty->assign('cp', $usuario->cp);
        }elseif(isset($_GET["class"])&&$_GET["class"]=="entrenador"){
            $control = new UserControlador();
            $resultado = $control->getUsuarioById($_GET["id"], "entrenador", "id_e");
            $usuario = $resultado[0];
            $smarty->assign("titulo","Datos del Entrenador");
            $smarty->assign('permisos', $usuario->administrador);
            $smarty->assign('grupo', $usuario->grupo);
        }
    }    
    
    $smarty->assign('nombre', $usuario->nombre);
    $smarty->assign('apellido', $usuario->apellidos); 
    $smarty->assign('dni', $usuario->dni);
    $smarty->assign('foto', $usuario->foto);
    $smarty->assign('email', $usuario->email);
    $smarty->assign('telefono', $usuario->telefono);
    $smarty->assign('entrenador',$usuario->entrenador);
    
    
    
    
    $smarty->display('principal/head.tpl');
    $smarty->display('entrenador/navEntrenador.tpl');       
    $smarty->display('entrenador/datosUsuario.tpl'); 
    $smarty->display('principal/footer.tpl');