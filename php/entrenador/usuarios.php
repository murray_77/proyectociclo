<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/UserControlador.class.php';
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if($_SESSION["usuario"]->entrenador!=1){
        header('Location: /proyecto2.0/php/misc/noautorizado.php');
    }

    $listaUsuarios = new UserControlador();
    if(isset($_GET["usuario"])&&$_GET["usuario"]=="cliente"){
        
        $resultado = $listaUsuarios->getAll("cliente","id_c");
        $smarty->assign("titulo","Clientes");
        $smarty->assign("cabecera","Clientes");
        $smarty->assign("direccion","forms/nuevoCliente.php");
    }elseif(isset($_GET["usuario"])&&$_GET["usuario"]=="entrenador"){
        $resultado = $listaUsuarios->getAll("entrenador","id_e");
        $smarty->assign("titulo","Entrenadores");
        $smarty->assign("cabecera","Entrenadores");
        $smarty->assign("direccion","forms/nuevoEntrenador.php");
    }
    
    $smarty->assign('usuarios', $resultado);   
    $smarty->display('principal/head.tpl');
    $smarty->display('entrenador/navEntrenador.tpl');       
    $smarty->display('entrenador/listaUsuarios.tpl');        
    $smarty->display('principal/footer.tpl');