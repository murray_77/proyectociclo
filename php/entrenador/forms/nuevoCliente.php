<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Validaciones.class.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/UserControlador.class.php'; 
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if(($_SESSION["usuario"]->entrenador!=1)||$_SESSION["usuario"]->administrador!=1){
        header('Location: /proyecto2.0/misc/noautorizado.php');
    }   
     
    
    function redirect($url){
        header("Location: $url");
    }
    /*
     * $error1 = "Debe introducir un nombre de usuario valido";
     * $error2 = "Debe introducir un apellido valido";
     * $error3 = "Debe introducir un dni valido"; 
     * $error4 = "Debe estar asignado a un grupo";
     * $error5 = "Debe introducir un numero de contacto";
     * $error6 = "Debe introducir un email";     
    */
    
    
    if(isset($_POST["aceptar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){ /*Validación para creacion*/
			
        $error1 = $error2 = $error3 = $error4 = $error5 = $error6 = false;

        $validar = new Validaciones();

        $nombre = $validar->validaNombre($_POST["nombre"]);
        $apellido = $validar->validaNombre($_POST["apellido"]);
        $dni = $validar->validaDni($_POST["dni"]);
        $grupo = $validar->validaGrupo($_POST["grupo"]);
        $telefono = $validar->validaTelefono($_POST["telefono"]);
        $email = $validar->validaEmail($_POST["email"]);            
        $fechaN = $_POST["fechaN"];
        $direccion = $_POST["dire"];
        $poblacion = $_POST["pob"];
        $cp = $_POST["cp"];
        $cMed = $_POST["cMed"];
        $anotaciones = $_POST["anotaciones"];
        $objetivos = $_POST["objetivos"];


        if (!$nombre){ $error1 = true; }						
        if (!$apellido){ $error2 = true; }						
        if (!$apellido){ $error3 = true; }            
        if (!$grupo){ $error4 = true; }           
        if (!$telefono){ $error5 = true; }            
        if (!$email){ $error6 = true; }           
            
            
						
        if($error1 == false && $error2 == false && $error3 == false && 
                $error4 == false && $error5== false && $error6 == false){
            
            $cliente = new Cliente;
            $cliente->setNombre($nombre);
            $cliente->setApellido($apellido);
            $cliente->setDni($dni);
            $cliente->setGrupoCliente($grupo);
            $cliente->setTelefono($telefono);
            $cliente->setEmail($email);
            $cliente->setPass($validar->generaPass());
            $cliente->setEntrenador(0);
            $cliente->setFechaN($fechaN);
            $cliente->setDireccion($direccion);
            $cliente->setPoblacion($poblacion);
            $cliente->setCp($cp);
            $cliente->setAnotaciones($anotaciones);
            $cliente->setCMedicas($cMed);
            $cliente->setObjetivos($objetivos);            
            
            $controlador = new UserControlador();
            $controlador->insertCliente($cliente);

            $url="../usuarios.php?usuario=cliente";

            redirect($url);

        }else{
            $arraryError = array();
            for($i=1;$i<=6;$i++){
                eval('$error=$error'.$i.";");
                if($error == true){
                    $arrayError[$i] = $error;                    
                }
            }
            $_SESSION["error"]=$arrayError;
        }
    }	
    
    if(isset($_SESSION["error"])){
        $smarty->assign("arrayError",$_SESSION["error"]); 
    }
       
    $smarty->assign("titulo","Alta Usuarios");
    $smarty->display('principal/head.tpl');    
    $smarty->display('entrenador/navEntrenador.tpl');   
    $smarty->display('entrenador/nuevoCliente.tpl');  
    $smarty->display('principal/footer.tpl');