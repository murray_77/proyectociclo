<?php

    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if(!isset($_SESSION["usuario"])||$_SESSION["usuario"]->entrenador!=0){
        header('Location: /proyecto2.0/php/misc/noautorizado.php');
    }
    
    $smarty->assign("titulo","Agenda");
    $smarty->display('principal/head.tpl');
    $smarty->display('cliente/navCliente.tpl');       
    $smarty->display('cliente/agendaCliente.tpl');    
    $smarty->display('principal/footer.tpl');