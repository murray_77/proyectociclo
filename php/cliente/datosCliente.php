<?php

    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/UserControlador.class.php');
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if(!isset($_SESSION["usuario"])||$_SESSION["usuario"]->entrenador!=0){
        header('Location: /proyecto2.0/php/misc/noautorizado.php');
    }
    
    $control = new UserControlador();
            $smarty->assign('nombre', $_SESSION["usuario"]->nombre);
            $smarty->assign('apellido', $_SESSION["usuario"]->apellidos); 
            $smarty->assign('dni', $_SESSION["usuario"]->dni);
            $smarty->assign('foto', $_SESSION["usuario"]->foto);
            $smarty->assign('email', $_SESSION["usuario"]->email);
            $smarty->assign('telefono', $_SESSION["usuario"]->telefono);
            $smarty->assign('entrenador',$_SESSION["usuario"]->entrenador);
            $smarty->assign('grupo', $_SESSION["usuario"]->grupo);    
            $smarty->assign('fechaN', $_SESSION["usuario"]->fechaNac);
            $smarty->assign('direccion', $_SESSION["usuario"]->direccion);
            $smarty->assign('poblacion', $_SESSION["usuario"]->poblacion);
            $smarty->assign('cp', $_SESSION["usuario"]->cp);
    
    $smarty->assign("titulo","Datos del Usuario");
    $smarty->display('principal/head.tpl');
    $smarty->display('cliente/navCliente.tpl');  
    $smarty->display('principal/datosUsuario.tpl');    
    $smarty->display('principal/footer.tpl');