<main class="contenedor">
    <h1 class="fila">Alta de Nuevo Entrenador</h1>
    
    <form action="{$smarty.server.PHP_SELF}" method="post" onsubmit="return validaUsuario();">
        <h3>Datos Personales</h3>
        <div class="form-row">
            <div class="form-group col-md-6">	
                <label for="nombre">Nombre</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="nombre" name="nombre" autofocus>
                <div class="invalid-feedback" id="error1"></div> 
            </div>
            <div class="form-group col-md-6">
                <label for="apellido">Apellidos</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="apellido" name="apellido">                
                <div class="invalid-feedback" id="error2"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="dni">DNI</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Ocho caracteres numéricos y una letra">
                <input type="text" class="form-control" id="dni" name="dni">                
                <div class="invalid-feedback" id="error3"></div>
            </div> 
            <div class="form-group col-md-6">    
                <label for="em">Email</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="ej: usuario@miemail.com, Se utilizará como nombre de usuario">
                <input type="email" class="form-control" id="em" name="em">
                <div class="invalid-feedback" id="error6"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="grupo">Grupo</label>
                <select name="grupo" id="grupo" class="form-control">
                    <option value=""></option>
                    <option value="G1">Grupo 1</option>
                    <option value="G2">Grupo 2</option>
                </select>
                <div class="invalid-feedback" id="error4"></div>
            </div>
        </div>
            
        <h3>Contacto</h3>
        <div class="form-row">              
            <div class="form-group col-md-6">    
                <label for="tele">Teléfono</label>
                <input type="number" class="form-control" id="telefono" name="tele">
                <div class="invalid-feedback" id="error5"></div>
            </div>
        </div>
            
            <h3>Acceso</h3>
                <div class="form-row">                 
                    <div class="form-group col-md-6">
                        <label for="permisos">Permisos de administrador</label>
                        <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Alguien con permisos de administrador puede modificar y crear nuevos datos de usuarios y entrenadores"></br>
			<input type="radio" class="" name="permisos" value="0" checked>No</br>
			<input type="radio" class="" name="permisos" value="1">Si</br>
                    </div>
		</div>
		<div class="form-group row">
                    <div class="col-sm-10">
                        <input type="submit" class="btn btn-dark" id="aceptar" name="aceptar" value="Aceptar">
                    </div>
		</div>
    </form>    
</main>