<header>
    <nav class="nav2">
        <div class="contenedorNav">
            <a href="/proyecto2.0/index.php">
                <img class="logoNav2" src="/proyecto2.0/img/icons/logo_nav.png" alt="logo">
            </a>
            <div class="navBoton" onclick="cambiar(this);">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>    
        <ul class="main-nav" id="menu">
            <li><a id="entrenadores" class="en2" href="/proyecto2.0/php/entrenador/usuarios.php?usuario=entrenador">ENTRENADORES</a></li>
            <li><a id="clientes" class="en2" href="/proyecto2.0/php/entrenador/usuarios.php?usuario=cliente">CLIENTES</a></li>
            <li><a id="grupos" class="en2" href="#">GRUPOS</a></li>
            <li><a id="agenda" class="en2" href="/proyecto2.0/php/entrenador/agenda.php">AGENDA</a></li>
            <li><a class="destacado" href="/proyecto2.0/clases/logoff.php">CERRAR SESION</a></li>
        </ul>        
    </nav>
</header>