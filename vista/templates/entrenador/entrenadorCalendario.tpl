<main class="container" onload="mostrarCalendario(actual.getFullYear(),actual.getMonth()+1)">		
    <h1>Gestión de Citas</h1>
    <div>
        <table id="calendario" class="table-bordered calendario">
			<caption></caption>
            <thead>
				<tr>
					<th>Lun</th>
					<th>Mar</th>
					<th>Mie</th>
					<th>Jue</th>
					<th>Vie</th>
					<th>Sab</th>
					<th>Dom</th>
				</tr>
            </thead>
            <tbody>
            </tbody>
		</table>
    </div>
   
</main>