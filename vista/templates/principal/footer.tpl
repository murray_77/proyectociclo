        <footer class="foot">
            <div class="foot01">
                <img class="logoFoot" src="/proyecto2.0/img/icons/logo_solo_blanco.png">
            </div>
            <div class="foot02">
                <img class="redes" src="/proyecto2.0/img/icons/redes.png">
                <p>Copyright 2020, Todos los derechos reservados</p>
            </div>
            <div class="foot03">
                <a href="#" class="enlaceFoot">PRIVACIDAD</a></li>
                <a href="#" class="enlaceFoot">PREGUNTAS FRECUENTES</a>
                <a href="#" class="enlaceFoot">COOKIES</a>
            </div>
        </footer>

        <script type="application/javascript" src="/proyecto2.0/js/jquery.js"></script>
        <script type="application/javascript" src="/proyecto2.0/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <script type="application/javascript" src="/proyecto2.0/fullcalendar/core/main.min.js"></script>
        <script type="application/javascript" src="/proyecto2.0/fullcalendar/core/locales/es.js"></script>
        <script type="application/javascript" src="/proyecto2.0/fullcalendar/daygrid/main.min.js"></script>
        <script type="application/javascript" src="/proyecto2.0/fullcalendar/timegrid/main.min.js"></script>
        <script type="application/javascript" src="/proyecto2.0/fullcalendar/interaction/main.min.js"></script>
        <script type="text/javascript" src="/proyecto2.0/slick/slick.min.js"></script><!--slider-->
        <script type="application/javascript" src="/proyecto2.0/js/script.js"></script>
    
    </body>
</html>
