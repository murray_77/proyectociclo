<header>
    <nav class="nav">
        <div class="backNav"></div>
        <div class="contenedorNav">
            <a href="/proyecto2.0/index.php">
                <img class="logoNav" src="/proyecto2.0/img/icons/logo_nav.png" alt="logo">
            </a>
            <a class="logo" href="/proyecto2.0/index.php">
                <img class="logoNavB" src="/proyecto2.0/img/icons/logo_solo_blanco.png" alt="logo">
            </a>
            <div class="navBoton" onclick="cambiar(this);">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>    
        <ul class="main-nav" id="menu">
            <li><a href="#presentacion" class="enlaceNav en1">INICIO</a></li>
            <li><a href="#tarifas" class="enlaceNav en1">TARIFAS</a></li>
            <li><a href="#blog" class="enlaceNav en1">BLOG</a></li>
            <li><a href="#contacto" class="enlaceNav en1">CONTACTO</a></li> 
            
        {if isset($smarty.session.usuario)}                        
            {if isset($smarty.session.usuario->entrenador)&&$smarty.session.usuario->entrenador == 1}
                <li><a class="enlaceNav en1" href="/proyecto2.0/php/entrenador/agenda.php">ADMINISTRAR</a></li>
            {else}
                <li><a class="enlaceNav en1" href="/proyecto2.0/php/cliente/agendaCliente.php">USUARIO</a></li>
            {/if}
            <li><a class="destacado" href="/proyecto2.0/clases/logoff.php">CERRAR SESION</a></li>
            <li><img class="fotoNav" src="/proyecto2.0/img/profiles/{$smarty.session.usuario->foto}"/></li>            
        {else}
            <li><a class="destacado" id="login" href="#">LOGIN</a></li>
        {/if}           
                    
        </ul>        
    </nav>
</header>