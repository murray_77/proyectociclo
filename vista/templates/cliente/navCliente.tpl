<header>
    <nav class="nav2">
        <div class="contenedorNav">
            <img class="logoNav2" src="/proyecto2.0/img/icons/logo_nav.png" alt="logo">
            <div class="navBoton" onclick="cambiar(this);">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>    
        <ul class="main-nav" id="menu">
            <li><a id="todose" class="enlaceNav2 en2" href="/proyecto2.0/php/entrenador/usuarios.php?usuario=entrenador">MI PERFIL</a></li>
            <li><a id="todos" class="enlaceNav2 en2" href="/proyecto2.0/php/entrenador/usuarios.php?usuario=cliente">PREGUNTAS FRECUENTES</a></li>
            <li><a id="agenda" class="enlaceNav2 en2" href="/proyecto2.0/php/entrenador/agenda.php">HORARIO</a></li>
            <li><a class="destacado" href="/proyecto2.0/clases/logoff.php">CERRAR SESION</a></li>
        </ul>        
    </nav>
</header>