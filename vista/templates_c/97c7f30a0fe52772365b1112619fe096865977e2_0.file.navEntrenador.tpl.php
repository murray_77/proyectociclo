<?php
/* Smarty version 3.1.33, created on 2020-04-29 14:09:48
  from 'C:\xampp\htdocs\proyecto2.0\vista\templates\entrenador\navEntrenador.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ea96e8c8e32a2_23338874',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '97c7f30a0fe52772365b1112619fe096865977e2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\proyecto2.0\\vista\\templates\\entrenador\\navEntrenador.tpl',
      1 => 1588162186,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ea96e8c8e32a2_23338874 (Smarty_Internal_Template $_smarty_tpl) {
?><header>
    <nav class="nav2">
        <div class="contenedorNav">
            <a href="/proyecto2.0/index.php">
                <img class="logoNav2" src="/proyecto2.0/img/icons/logo_nav.png" alt="logo">
            </a>
            <div class="navBoton" onclick="cambiar(this);">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>    
        <ul class="main-nav" id="menu">
            <li><a id="entrenadores" class="en2" href="/proyecto2.0/php/entrenador/usuarios.php?usuario=entrenador">ENTRENADORES</a></li>
            <li><a id="clientes" class="en2" href="/proyecto2.0/php/entrenador/usuarios.php?usuario=cliente">CLIENTES</a></li>
            <li><a id="grupos" class="en2" href="#">GRUPOS</a></li>
            <li><a id="agenda" class="en2" href="/proyecto2.0/php/entrenador/agenda.php">AGENDA</a></li>
            <li><a class="destacado" href="/proyecto2.0/clases/logoff.php">CERRAR SESION</a></li>
        </ul>        
    </nav>
</header><?php }
}
