<?php
/* Smarty version 3.1.33, created on 2020-02-15 05:29:44
  from 'C:\wamp64\www\proyecto2.0\vista\templates\entrenador\nuevoEntrenador.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4781c8a09725_30164852',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c12eb733d8f459327890d9e3dd642a2551a57a2' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\entrenador\\nuevoEntrenador.tpl',
      1 => 1581706791,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4781c8a09725_30164852 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="container">
    <h1 class="">Alta de Nuevo Entrenador</h1>
    <form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="post" onsubmit="return validaUsuario();">
        <h3>Datos Personales</h3>
        <div class="form-row">
            <div class="form-group col-md-6">	
                <label for="nombre">Nombre</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="nombre" name="nombre" autofocus>
                <div class="invalid-feedback" id="error1"></div> 
            </div>
            <div class="form-group col-md-6">
                <label for="apellido">Apellidos</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="apellido" name="apellido">                
                <div class="invalid-feedback" id="error2"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="dni">DNI</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Ocho caracteres numéricos y una letra">
                <input type="text" class="form-control" id="dni" name="dni">                
                <div class="invalid-feedback" id="error3"></div>
            </div> 
            <div class="form-group col-md-6">    
                <label for="em">Email</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="ej: usuario@miemail.com, Se utilizará como nombre de usuario">
                <input type="email" class="form-control" id="em" name="em">
                <div class="invalid-feedback" id="error6"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="grupo">Grupo</label>
                <select name="grupo" id="grupo" class="form-control">
                    <option value=""></option>
                    <option value="G1">Grupo 1</option>
                    <option value="G2">Grupo 2</option>
                </select>
                <div class="invalid-feedback" id="error4"></div>
            </div>
        </div>
            
        <h3>Contacto</h3>
        <div class="form-row">              
            <div class="form-group col-md-6">    
                <label for="tele">Teléfono</label>
                <input type="number" class="form-control" id="telefono" name="tele">
                <div class="invalid-feedback" id="error5"></div>
            </div>
        </div>
            
            <h3>Acceso</h3>
                <div class="form-row">                 
                    <div class="form-group col-md-6">
                        <label for="permisos">Permisos de administrador</label>
                        <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Alguien con permisos de administrador puede modificar y crear nuevos datos de usuarios y entrenadores"></br>
			<input type="radio" class="" name="permisos" value="0" checked>No</br>
			<input type="radio" class="" name="permisos" value="1">Si</br>
                    </div>
		</div>
		<div class="form-group row">
                    <div class="col-sm-10">
                        <input type="submit" class="btn btn-dark" id="aceptar" name="aceptar" value="Aceptar">
                    </div>
		</div>
    </form>    
</main><?php }
}
