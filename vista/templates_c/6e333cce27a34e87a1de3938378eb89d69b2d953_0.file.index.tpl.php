<?php
/* Smarty version 3.1.33, created on 2020-02-05 04:47:39
  from 'C:\wamp64\www\proyecto2.0\vista\templates\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e3a48eb3b40e1_71999240',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e333cce27a34e87a1de3938378eb89d69b2d953' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\index.tpl',
      1 => 1579881386,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3a48eb3b40e1_71999240 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="container">
    <h1>Titulo</h1>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                    <img src="img/cont1.jpg" alt="contenido" class="imagen"><!--ilustracion de contenido alineada izquierda-->
            </div>
            <div class="col-sm-6">
                <p class="parrafo text-sm-left">
                    Este es el texto 1 elit auctor morbi lobortis eget tristique litora, 
                    quis nullam conubia elementum habitasse euismod arcu eros nunc curabitur ad. Ut nisi vulputate molestie 
                    vitae interdum feugiat neque, placerat arcu facilisi ridiculus praesent metus auctor, cubilia hendrerit 
                    pretium euismod viverra sed. Ridiculus egestas condimentum aliquet aliquam nostra molestie dis auctor sollicitudin, 
                    imperdiet integer ante id aptent eget habitasse fames vehicula lobortis, per parturient vel taciti non volutpat etiam cras.
                </p>
            </div>
        </div>	

        <div class="row">
            <div class="col-sm-6">
                <p class="parrafo text-sm-right">
                    Este es el texto 2 elit auctor morbi lobortis eget tristique litora, 
                    quis nullam conubia elementum habitasse euismod arcu eros nunc curabitur ad. Ut nisi vulputate molestie 
                    vitae interdum feugiat neque, placerat arcu facilisi ridiculus praesent metus auctor, cubilia hendrerit 
                    pretium euismod viverra sed. Ridiculus egestas condimentum aliquet aliquam nostra molestie dis auctor sollicitudin, 
                    imperdiet integer ante id aptent eget habitasse fames vehicula lobortis, per parturient vel taciti non volutpat etiam cras.
                </p>
            </div>
            <div class="col-sm-6">
                <img src="img/cont2.jpg" alt="contenido" class="imagen"><!--ilustracion de contenido alineada derecha-->
            </div>
        </div>
    </div>
</main><?php }
}
