<?php
/* Smarty version 3.1.33, created on 2020-04-30 17:12:50
  from 'C:\xampp\htdocs\proyecto2.0\vista\templates\entrenador\listaUsuarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5eaaeaf2163863_99251652',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '744401b965860fc8722b7d5d949d0ce7b991d43d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\proyecto2.0\\vista\\templates\\entrenador\\listaUsuarios.tpl',
      1 => 1588255240,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:entrenador/tablaUsuarios.tpl' => 1,
  ),
),false)) {
function content_5eaaeaf2163863_99251652 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="contenedor">
    <div class="tituloBack">
        <h1>Lista de <?php echo $_smarty_tpl->tpl_vars['cabecera']->value;?>
</h1>

        <input type="text" class="busc" id="busc" onkeyup="buscar();" placeholder="Buscar Usuario" title="Buscar">
    </div>
        
    <div class="tablaWrap">    
        <?php $_smarty_tpl->_subTemplateRender('file:entrenador/tablaUsuarios.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </div>
    
    <div class="nUser">
        <a href=<?php echo $_smarty_tpl->tpl_vars['direccion']->value;?>
><img src="/proyecto2.0/img/icons/nUser.png"></a>
    </div>

</main>
    <?php }
}
