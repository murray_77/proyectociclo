<?php
/* Smarty version 3.1.33, created on 2020-05-01 16:52:49
  from 'C:\xampp\htdocs\proyecto2.0\vista\templates\entrenador\datosUsuario.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5eac37c1cfcfb4_63569922',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9799fc10c2314ef838318969793351ee31f729d6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\proyecto2.0\\vista\\templates\\entrenador\\datosUsuario.tpl',
      1 => 1588344768,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5eac37c1cfcfb4_63569922 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="capa2"></div>
<main class="contenedor datos">    
    <section class="encabezado">        
        <img class="userfoto" src="/proyecto/img/profiles/<?php echo $_smarty_tpl->tpl_vars['foto']->value;?>
" alt="Imagen Usuario"><!--Imagen actual del user o marco vacío-->
        <div class="titDetalles">
            <button type="button" id="botDatos" class="botonSimple active">Datos Personales</button>
            <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
            <button type="button" id="botAnotaciones" class="botonSimple">Anotaciones</button>
            <?php }?>
        </div>
        <div class="linea2">&nbsp;</div>
    </section> 
        
            
    
    <section id="datos" class="columnaGrande">
        <div class="fila">
            <div class="columna">

                <div class="itemCaja">
                    <h4 class="etiqueta">Nombre</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['nombre']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['apellido']->value;?>
</span>
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">DNI</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
</span>
                </div>

                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="itemCaja">                
                    <h4 class="etiqueta">Fecha Nac.</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['fechaN']->value;?>
</span>                
                </div>
                <?php }?>

            </div>


            <div class="columna">

                <div class="itemCaja">
                    <h4 class="etiqueta">Teléfono</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['telefono']->value;?>
</span>
                </div>
                <div class="itemCaja">
                    <h4 class="etiqueta">Email</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</span>
                </div>
                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="itemCaja">
                    <h4 class="etiqueta">Grupo</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
</span>
                </div>
                <?php }?>
            </div>           

            <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
             <div class="columna">   
                <div class="itemCaja">
                    <h4 class="etiqueta">Dirección</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['direccion']->value;?>
</span>
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">Población</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['poblacion']->value;?>
</span>                
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">CP</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['cp']->value;?>
</span>
                </div>
             </div>

            <?php } else { ?>

            <div class="columna">    
                <div class="itemCaja">                    
                    <h4 class="etiqueta">Grupo</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
</span>
                </div> 

                <div class="itemCaja">
                    <h4 class="etiqueta">Permisos de Administrador</h4>                
                    <span><?php echo $_smarty_tpl->tpl_vars['permisos']->value;?>
</span>
                </div>  
            </div>
            <?php }?> 
        </div>    
    </section>
        
    <section id="anotaciones" class="columnaGrande">
        <div class="fila">
            <div class="columna">   
                <div class="itemCaja">
                    <h4 class="etiqueta">Nombre</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['nombre']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['apellido']->value;?>
</span>
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">DNI</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
</span>
                </div>

                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="itemCaja">                
                    <h4 class="etiqueta">Fecha Nac.</h4>
                    <span><?php echo $_smarty_tpl->tpl_vars['fechaN']->value;?>
</span>                
                </div>
                <?php }?>
            </div>    
        </div>
        <div class="columna">
            
        </div>
        
    </section>    

    <?php if (isset($_SESSION['usuario']) && $_SESSION['usuario']->administrador == 1) {?>            
    <section class="formulario">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="post" class="botones">
            <input type="hidden" name="dni" value="<?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
">
            <input type="submit" class="boton destacado" name="eliminarC" value="Eliminar">
            <input type="submit" class="boton destacado" value="Editar">
        </form>
    </section>
    <?php }?>
    
</main><?php }
}
