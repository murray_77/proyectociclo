<?php
/* Smarty version 3.1.33, created on 2020-02-04 18:06:34
  from 'C:\wamp\www\proyecto2.0\vista\templates\entrenador\navEntrenador.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e39b2aa72ae87_10218174',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2046707b5d4133d8ad420147bc1704a318261aa1' => 
    array (
      0 => 'C:\\wamp\\www\\proyecto2.0\\vista\\templates\\entrenador\\navEntrenador.tpl',
      1 => 1580839587,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e39b2aa72ae87_10218174 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img class="minilogo navbar-brand" src="/proyecto2.0/img/logo.png" alt="logo">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="/proyecto2.0/index.php">Inicio</a></li>
            <li class="nav-item"><a id="agenda" class="nav-link" href="/proyecto2.0/php/entrenador/agenda.php">Agenda</a></li>
            
            <li class="nav-item dropdown-show btn-group"><a class="nav-link dropdown-toggle" id="cliente" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gestion de Clientes</a>
                <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">                    
                    <?php if (($_SESSION['usuario']->administrador) == 1) {?>    
                        <li class="dropdown-item bg-dark"><a id="nuevo" class="nav-link" href="/proyecto2.0/php/entrenador/forms/nuevoCliente.php">Nuevo Cliente</a></li>
                    <?php }?>
                        <li class="dropdown-item bg-dark"><a id="todos" class="nav-link" href="/proyecto2.0/php/entrenador/clientes.php">Lista de Clientes</a></li>
                        <li class="dropdown-item bg-dark"><a id="buscar" class="nav-link" href="/proyecto2.0/admin/subadmin/subaduser/buscador.php">Buscador de Clientes</a></li>
                </ul>
            </li>
            
            <li class="nav-item dropdown-show btn-group"><a class="nav-link dropdown-toggle" id="entrenador" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gestion de Entrenadores</a>
                <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">
                    <?php if (($_SESSION['usuario']->administrador) == 1) {?>    
                        <li class="dropdown-item bg-dark"><a id="nuevoe" class="nav-link" href="/proyecto2.0/php/entrenador/forms/nuevoEntrenador.php">Nuevo Entrenador</a></li>
                    <?php }?>
                        <li class="dropdown-item bg-dark"><a id="todose" class="nav-link" href="/proyecto2.0/php/entrenador/entrenadores.php">Lista de Entrenadores</a></li>
                </ul>
            </li>
            
            <li class="nav-item dropdown-show btn-group"><a class="nav-link dropdown-toggle" id="grupos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Grupos</a>
                <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">
                    <?php if (($_SESSION['usuario']->administrador) == 1) {?>
                        <li class="dropdown-item bg-dark"><a id="nuevog" class="nav-link" href="/proyecto2.0/php/entrenador/forms/nuevoGrupo.php">Nuevo Grupo</a></li>
                    <?php }?>
                        <li class="dropdown-item bg-dark"><a id="todosg" class="nav-link" href="/proyecto2.0/php/entrenador/grupos.php">Ver Grupos</a></li>
                </ul>
            </li>
            
            <li class="nav-item"><a id="calendario" class="nav-link" href="/proyecto2.0/admin/subadmin/admincalendario.php">Calendario</a></li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="/proyecto2.0/clases/logoff.php">Cerrar Sesión</a></li>
        </ul>
    </div>
</nav><?php }
}
